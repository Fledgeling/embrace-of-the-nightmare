﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
	//Скорость, с которой мы поворачиваемся. По сути чувствительность мышки
	[SerializeField] float turnSpeed = 90f;

	//На сколько голову можно поднять вверх (в градусах). Определяет верхнюю границу.
	[SerializeField] float headUpperAngleLimit = 85f;

	//На сколько голову можно опустить вниз (в градусах). Определяет нижнюю границу.
	[SerializeField] float headLowerAngleLimit = -80f;

	//Текущий поворот головы в градусах
	float yaw = 0f; //по вертикальной оси
	float pitch = 0f; //по горизонтальной оси

	//Кватернион обозначает вращение и состоит из координат вращения и вектора.
	//Определяет ориентацию головы и тела в начале. Будет давать нам новые направления после суммирования с yaw и pitch
	Quaternion bodyStartOrientation;
	Quaternion headStartOrientation;

	//Ссылка на объект, который будем вращать, т.е. камеру. Сам скрипт у нас висит в теле, поэтому на него ссылка нам не нужна.
	Transform head;

    // Start говорит, что нужно сделать на старте игры. Просто
    void Start()
    {
    	//Ищем нашу голову по признаку "внутри этого объекта И камера"
        head = GetComponentInChildren<Camera>().transform;

        //Задаем ориентацию тела и головы = какая есть на начало сцены
        bodyStartOrientation = transform.localRotation;
        headStartOrientation = head.transform.localRotation;

        //закрепляем курсор в середине экрана и прячем его
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update вызывается каждый кадр. Поэтому когда фпс проседает или резко растет, апдейты тоже вызываются не каждую секунду, а когда меняется кадр.
    // Поэтому использовать мы его не будем
    void Update()
    {
        
    }

    //FixedUpdate вызывается строго одинаковое количество раз в секунду, поэтому он нам для этих целей подходит гораздо лучше
    void FixedUpdate()
    {
    	//Берем движение мышки, умножаем на прошедшее время и скорость поворота.
    	//Так получаем значение поворота по горизонтали и вертикали.
        var horizontal = Input.GetAxis("Mouse X") * Time.deltaTime * turnSpeed;
        var vertical = -Input.GetAxis("Mouse Y") * Time.deltaTime * turnSpeed;

        //Обновляем yaw и pitch соответственно
        yaw += horizontal;
        pitch += vertical;

        //Ограничиваем движение камеры вверх и вниз строго до установленных нами границ
        pitch = Mathf.Clamp(pitch, headLowerAngleLimit, headUpperAngleLimit);

        //Считаем, на сколько нужно повернуть тело относительно оси Y, и голову относительно оси X
        var bodyRotation = Quaternion.AngleAxis(yaw, Vector3.up);
        var headRotation = Quaternion.AngleAxis(pitch, Vector3.right);

        transform.localRotation = bodyRotation * bodyStartOrientation;
        head.localRotation = headRotation * headStartOrientation;

    }
}
