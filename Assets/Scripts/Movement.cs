﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    //Скорость передвижения, в единицах в секунду.
    [SerializeField] float moveSpeed = 5f;

    //Высота прыжка, в единицах
    [SerializeField] float jumpHeight = 20f;

    //Скорость, с которой будет уменьшаться вертикальная скорость, в единицах в секунду
    [SerializeField] float gravity = 20f;

    //ВАЖНО! После сериализации эти три параметра берут значения не отсюда, а из самого Unity, и менять их нужно тоже там!

    //Насколько мы можем контроллировать тело в воздухе, в значении от 0 до 10
    [Range(0, 10), SerializeField] float airControl = 5f;

    //Текущее направление движения. Тут же приравниваем его к нулевому вектору - по умолчанию мы никуда не движемся
    Vector3 moveDirection = Vector3.zero;

    //Назовем как-нибудь наш компонент CharacterController, потому что будем к нему часто обращаться.
    CharacterController controller;

    // Start is called before the first frame update
    void Start()
    {
        //На старте мы уточним, что нужно взять компонент CharacterController этого объекта
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Желаемое направление движения игрока. На земле оно просто равно направлению движения, 
        //а в воздухе мы будем интерполировать между ним и вектором нашего текущего передвижения, чтобы имитировать инерцию
        var input = new Vector3(
            Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical")
        );

        input *= moveSpeed;

        //Метод Move у Character Controller использует глобальные направления, 
        //поэтому мы конвертируем полученный вектор в глобальное направление
        input = transform.TransformDirection(input);

        //Определяем, касается ли controller нашего игрока земли
        if (controller.isGrounded)
        {
            //Определяем величину вектора, с которым мы хотим двигаться
            moveDirection = input;

            //Игрок жмет прыжок?
            if (Input.GetButton("Jump"))
            {
                // Значит игрок пытается прыгнуть.
                // Считаем, сколько нам нужно вертикальной скорости применить к игроку.
                // При этом стараемся учесть гравитацию и высоту прыжка
                moveDirection.y = Mathf.Sqrt(2 * gravity * jumpHeight);
            }
            else
            {
                //Игрок на земле и не хочет прыгать. Тогда вертикальное направление приравниваем к нулю
                //Если мы этого не делаем, то у игрока будет накапливаться -гравитация каждый фрейм, 
                //и как только он окажется в воздухе, его с силой швырнет в землю
                moveDirection.y = 0;
            }
        }
        else
        {
            //Медленно приблежаем текущее движение к желаемому игроком
            //Но сохраняем движение по вертикали, чтобы дуга прыжка оставалась неизменной
            input.y = moveDirection.y;
            moveDirection = Vector3.Lerp(moveDirection, input, airControl * Time.deltaTime);
        }

        //Постоянно уменьшаем наше движение за счет гравитации
        moveDirection.y -= gravity * Time.deltaTime;
        Debug.Log("moveDirection.y=" + moveDirection.y);

        //Двигаем персонажа
        controller.Move(moveDirection * Time.deltaTime);
    }
}
